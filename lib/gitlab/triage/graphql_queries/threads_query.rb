module Gitlab
  module Triage
    module GraphqlQueries
      ThreadsQuery = <<-GRAPHQL.freeze # rubocop:disable Naming/ConstantName
        query($source: ID!, $after: String, $iids: [String!]) {
          %{source_type}(fullPath: $source) {
            id
            %{resource_type}(after: $after, iids: $iids%{group_query}) {
              pageInfo {
                hasNextPage
                endCursor
              }
              nodes {
                id
                userDiscussionsCount
              }
            }
          }
        }
      GRAPHQL
    end
  end
end
