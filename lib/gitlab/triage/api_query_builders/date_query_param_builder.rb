require_relative '../validators/params_validator'

module Gitlab
  module Triage
    module APIQueryBuilders
      class DateQueryParamBuilder
        ATTRIBUTES = %w[updated_at created_at].freeze
        CONDITIONS = %w[older_than newer_than].freeze
        INTERVAL_TYPES = %w[days weeks months years].freeze

        def self.filter_parameters
          [
            {
              name: :attribute,
              type: String,
              values: ATTRIBUTES
            },
            {
              name: :condition,
              type: String,
              values: CONDITIONS
            },
            {
              name: :interval_type,
              type: String,
              values: INTERVAL_TYPES
            },
            {
              name: :interval,
              type: Numeric
            }
          ]
        end

        def self.applicable?(condition)
          ATTRIBUTES.include?(condition[:attribute].to_s)
        end

        def initialize(condition_hash)
          @attribute = condition_hash[:attribute].to_s
          @interval_condition = condition_hash[:condition].to_sym
          @interval_type = condition_hash[:interval_type]
          @interval = condition_hash[:interval]
          validate_condition(condition_hash)
        end

        def validate_condition(condition)
          ParamsValidator.new(self.class.filter_parameters, condition).validate!
        end

        def param_name
          prefix = attribute.sub(/_at\z/, '')
          suffix =
            case interval_condition
            when :older_than
              'before'
            when :newer_than
              'after'
            end

          "#{prefix}_#{suffix}"
        end

        def param_content
          interval.public_send(interval_type).ago.to_date # rubocop:disable GitlabSecurity/PublicSend
        end

        def build_param
          "&#{param_name}=#{param_content.strip}"
        end

        private

        attr_reader :condition_hash, :attribute, :interval_condition, :interval_type, :interval
      end
    end
  end
end
